#!/bin/python
import random
from controller import *
import string
import urlparse
import urllib
import json, ast
import time,datetime
from random import random,choice,sample,randint
import os
import threading
#from pymongo import pymongo
from kafka import KafkaProducer
from kafka.errors import KafkaError
import json
from ua_parser import user_agent_parser
import UserAgentList
from Specification import *
import pprint
pp = pprint.PrettyPrinter(indent=4)

DEEPLINK_URL_BASE = "https://dl.merakart.com/"
IMAGE_URL_BASE = "https://img-cdn.merakart.com/"
PRODUCT_URL_BASE = "https://merakart.com/"
MOBILE_URL_BASE = "https://m.merakart.com/"


producer = KafkaProducer(
    bootstrap_servers=[ "139.59.31.208:9093" , "139.59.14.13:9093", "139.59.31.208:9092" , "139.59.14.13:9092" ],
    retries=5, 
    value_serializer=lambda m: json.dumps(m).encode('ascii'))


def random_stuff(n):
    return ''.join([choice(string.ascii_letters + string.digits) for n in xrange(n)])

#CAMPAIGN_TYPE = ["Email", "SMS", ""]
CATEGORIES = ["Clothing", "Footwear", "Electronics", "Stationary", "Home-Decor"]
PRODUCT = [
        ["tshirt", "jeans", "shirt", "socks"],
        ["laptop" , "mobile", "television", "speaker"],
        ["pen" , "pencil", "earser", "marker"],
        ["sport-shoe" , "loafer", "sandals", "slippers"],
        ["curtain" , "lamp", "chair", "bed"]
        ]import threading
TAG = [
        ["blue", "red", "green" , "slim" , "fit" , "full", "half" , "3/4th", "buttons", "backless"],
        ["italian", "sport", "football", "party", "formal"],
        ["premium" , "black", "space-gray", "some random stuff" , "some random tag"],
        ["black", "blue", "red", "green", "yellow"],
        ["double", "single", "foldable", "washable"]
    ]

def trigger():
    for i in range(50):
        temp_record_dict = {
        "id" : "",
        "product_id" : "",
        "variant_id" : "",
        "sku" : "",
        "name" : "",
        "title" : "",
        "campaign_id" : "",
        "campaign_type" : "",
        "description" : "",
        "product_url" : "",
        "image_url" : "",
        "mobile_url" : "",
        "deeplink_url" : "",
        "specification" : {},
        "category" : "",
        "tags" : [],
        "is_active" : "",
        "cost_price" : "",
        "selling_price" : "",
        "mrp" : "",
        "discounted_price" : "",
        "shipping_cost" : "",
        "stock_avalibility" : "",
        "stock_quantity" : "",
        "timestamp" : "",
        "activity" : "",
        "activity_metadata" : {

        },
        "ip_address" : "",
        "user_agent" : "",
        "location" : {
        },
        "device" : {
            "device_type" : "",
            "device_brand" : "",
            "device_model" : "",
            "device_os" : "",
            "device_os_version" : "",
            "device_broswer" : "",
            "device_browser_version" : "",
            }
        }

        temp_record_dict["id"] = random_stuff(20)
        temp_record_dict["product_id"] = random_stuff(15)
        temp_record_dict["variant_id"] = random_stuff(10)
        temp_record_dict["sku"] = random_stuff(14)
        index = CATEGORIES.index(choice(CATEGORIES))
        item_index = randint(0,len(PRODUCT[index]) - 1)
        temp_record_dict["name"] = PRODUCT[index][item_index] + "_" + random_stuff(6)
        temp_record_dict["title"] = PRODUCT[index][item_index] + "_" + random_stuff(6)
        specs = get_entity_spec(PRODUCT[index][item_index])
        temp_record_dict["specification"] = specs
        temp_record_dict["campaign_id"] = 100
        temp_record_dict["category"] = CATEGORIES[index]
        temp_record_dict["campaign_type"] = "add_product"
        temp_record_dict["description"] = " ".join([random_stuff(7) for i in range(20)])
        temp_record_dict["product_url"] = PRODUCT_URL_BASE + random_stuff(6)
        temp_record_dict["image_url"] = IMAGE_URL_BASE + random_stuff(6)
        temp_record_dict["mobile_url"] = MOBILE_URL_BASE + random_stuff(6) 
        temp_record_dict["deeplink_url"] = DEEPLINK_URL_BASE + random_stuff(6)
        temp_record_dict["tags"] = sample(TAG[index],3)
        temp_record_dict["is_active"] = True
        temp_record_dict["cost_price"] = randint(20,20000)
        temp_record_dict["selling_price"] = temp_record_dict["cost_price"] * (1 + randint(1,100)/100.0)
        temp_record_dict["mrp"] = temp_record_dict["selling_price"] * (1 + randint(1,5)/100.0) 
        temp_record_dict["discounted_price"] = temp_record_dict["cost_price"] * (1 + randint(1,100)/100.0)
        temp_record_dict["shipping_cost"] = randint(20,40)
        temp_record_dict["stock_avalibility"] = choice([True, False])
        temp_record_dict["stock_quantity"] = randint(10,1000)
        temp_record_dict["timestamp"] = int(time.time()*1000)
        temp_record_dict["activity"] = "add_product"
        temp_record_dict["ip_address"] = ".".join([str(randint(2,222)) for i in range(4)])
        ip_record = getDataForIP(temp_record_dict["ip_address"])
        temp_record_dict["location"] = ip_record
        #temp_record_dict["ip_address"] = choice(["123.456.234.789", "34.56.234.77", "234.67.234.56"])
        temp_record_dict["user_agent"] = choice(UserAgentList.USER_AGENT_LIST)
        # temp_record_dict["location"]["country_short"] = ip_record.country_short
        # temp_record_dict["location"]["country_long"] = ip_record.country_long
        # temp_record_dict["location"]["region"] = "DELHI-NCR"
        # temp_record_dict["location"]["city"] = "GURGAON"

        uaparse = user_agent_parser.Parse(temp_record_dict["user_agent"])
        temp_record_dict["device"]["device_broswer"] = uaparse["user_agent"]["family"]
        temp_record_dict["device"]["device_os"] = uaparse["os"]["family"]
        temp_record_dict["device"]["device_brand"] = uaparse["device"]["brand"]
        temp_record_dict["device"]["device_type"] = uaparse["device"]["family"]
        temp_record_dict["device"]["device_os_version"] = uaparse["os"]["major"]
        temp_record_dict["device"]["device_browser_version"] = uaparse["user_agent"]["family"]
        temp_record_dict["activity_metadata"]["uploaded_by"] = choice(["CSV", "XML", "RSS"])

        future = producer.send('e-comm-test-2',value=temp_record_dict)
        record_metadata = future.get(timeout=10)
        print (record_metadata.offset)

        temp_record_dict["activity_metadata"] = {}
        for i in range(10000):
            user_id = random_stuff(10)
            for kk in sample(["sent", "viewed", "delivered", "clicked", "add to cart", "abandoned", "purchased"],4):
                channel = choice(["DPN", "Mobile Push", "SMS", "On-Site", "E-mail"])
                ip_record = getDataForIP(temp_record_dict["ip_address"])
                temp_record_dict["location"] = ip_record
                #temp_record_dict["ip_address"] = choice(["123.456.234.789", "34.56.234.77", "234.67.234.56"])
                temp_record_dict["user_agent"] = choice(UserAgentList.USER_AGENT_LIST)    
                uaparse = user_agent_parser.Parse(temp_record_dict["user_agent"])
                temp_record_dict["device"]["device_broswer"] = uaparse["user_agent"]["family"]
                temp_record_dict["device"]["device_os"] = uaparse["os"]["family"]
                temp_record_dict["device"]["device_brand"] = uaparse["device"]["brand"]
                temp_record_dict["device"]["device_type"] = uaparse["device"]["family"]
                temp_record_dict["device"]["device_os_version"] = uaparse["os"]["major"]
                temp_record_dict["device"]["device_browser_version"] = uaparse["user_agent"]["major"]
                temp_record_dict["timestamp"] = int(time.time()*1000)
                temp_record_dict["activity"] = kk
                temp_record_dict["activity_metadata"]["channel"] = channel
                temp_record_dict["activity_metadata"]["token"] = random_stuff(9)
                temp_record_dict["activity_metadata"]["user_id"] = user_id

                if kk in ["add to cart", "abandoned", "purchased"]:
                    temp_record_dict["activity_metadata"]["session_id"] = random_stuff(10)
                    temp_record_dict["activity_metadata"]["transaction_id"] = random_stuff(9)

                future = producer.send('e-comm-test-2',value=temp_record_dict)
                record_metadata = future.get(timeout=10)
                print (record_metadata.offset)


            

#                future = producer.send('my-topic-json-test-v2',value=message )
# try:
#     #pass 
#     record_metadata = future.get(timeout=10)
# except KafkaError,e:
# # Decide what to do if produce request failed...
#     #log.exception()
#     print e
#     pass

# # Successful result returns assigned partition and offset
# print (record_metadata.topic)
# print (record_metadata.partition)
# print (record_metadata.offset)
if __name__ == "__main__":
    threads = [ threading.Thread(target=trigger) for i in range(int(sys.argv[1]))]
    [t.start() for t in threads]