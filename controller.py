from IPLocation import IP2Location
import os

database = IP2Location(os.path.join("data", "IP-COUNTRY-REGION-CITY-LATITUDE-LONGITUDE-ZIPCODE.BIN"))

def getDataForIP(ip):
	print ip
	if (len(ip.split(".")) == 4):
		print "true"
		rec = database.get_all(ip)
		dict_rec = {
			"country_short" : rec.country_short,
			"country_long" : rec.country_long,
			"region" : rec.region,
			"city" : rec.city,
			"isp" : rec.isp,
			"latitude" : rec.latitude,
			"longitude" : rec.longitude,
			"domain" : rec.domain,
			"zipcode" : rec.zipcode,
			"timezone" : rec.timezone,
			"netspeed" : rec.netspeed,
			"idd_code" : rec.idd_code,
			"area_code" : rec.area_code,
			"weather_code" : rec.weather_code,
			"weather_name" : rec.weather_name,
			"mcc" : rec.mcc,
			"mnc" : rec.mnc,
			"mobile_brand" : rec.mobile_brand,
			"elevation" : rec.elevation,
			"usage_type" : rec.usage_type
		}
		print dict_rec
		print type(dict_rec)
		return dict_rec

	else:
		print "false"
		return "invalid ip address"

# print(rec.country_short)
# print(rec.country_long)
# print(rec.region)
# print(rec.city)
# print(rec.isp)	
# print(rec.latitude)
# print(rec.longitude)			
# print(rec.domain)
# print(rec.zipcode)
# print(rec.timezone)
# print(rec.netspeed)
# print(rec.idd_code)
# print(rec.area_code)
# print(rec.weather_code)
# print(rec.weather_name)
# print(rec.mcc)
# print(rec.mnc)
# print(rec.mobile_brand)
# print(rec.elevation)
# print(rec.usage_type)
