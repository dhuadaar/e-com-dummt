from random import choice, sample

def get_entity_spec(key): 
	specification_dict = {
		"tshirt" : {
			"color" : choice(["red", "yellow", "green", "blue", "orange", "pink"]),
			"fabric" : choice(["cotton", "woollen", "linen", "nylon", "polyster"]),
			"size" : sample([34, 36, 38, 40, 42, 44, 46, 48], 5),
			"pattern" : choice(["checker", "plain", "solid", "beach", "strips"]),
			"sleeves" : choice(["full", "half", "cut"]),
			"fit" : choice(["regular", "slim", "narrow"]),
			"brand" : choice(["Numero-Uno", "Levis", "Spykar", "Puma", "Allen Solly"])
		}, 
		"jeans" : {
			"color" : choice(["red", "yellow", "green", "blue", "orange", "pink"]),
			"fabric" : choice(["demin", "Cotton Blend", "cotton", "raw"]),
			"size" : sample([34, 36, 38, 40, 42, 44, 46, 48], 6),
			"pattern" : choice(["acid-wash", "plain", "solid", "wear-tear", "monkey-wash"]),
			"fit" : choice(["regular", "slim", "narrow"]),
			"brand" : choice(["Numero-Uno", "Levis", "Spykar", "Puma", "Allen Solly"])
		}, 
		"shirt" : {
			"color" : choice(["red", "yellow", "green", "blue", "orange", "pink"]),
	    	"fabric" : choice(["demin", "Cotton Blend", "cotton", "linen"]),
			"size" : sample([34, 36, 38, 40, 42, 44, 46, 48], 6),
			"pattern" : choice(["checker", "plain", "solid", "beach", "strips"]),
			"sleeves" : choice(["full", "half", "cut"]),
			"fit" : choice(["regular", "slim", "narrow"]),
			"brand" : choice(["Numero-Uno", "Levis", "Spykar", "Puma", "Allen Solly"])
		}, 
		"socks" : {
			"color" : choice(["red", "yellow", "green", "blue", "orange", "pink"]),
			"fabric" : choice(["cotton", "woollen", "nylon", "polyster"]),
			"size" : sample([8,10,12,14,16], 3),
			"brand" : choice(["Numero-Uno", "Levis", "Spykar", "Puma", "Allen Solly"])
		},
		"sport-shoe" : {
			"color" : choice(["red", "yellow", "green", "blue", "orange", "pink"]),
			"size" : sample([8,10,12,14,16], 3),
			"brand" : choice(["Puma", "Reebok", "Adidas", "Bata", "Lotto"])
		} , 
		"loafer" : {
			"color" : choice(["red", "yellow", "green", "blue", "orange", "pink"]),
			"size" : sample([8,10,12,14,16], 3),
			"brand" : choice(["Puma", "Reebok", "Adidas", "Bata", "Lotto"])
		}, 
		"sandals" : {
			"color" : choice(["red", "yellow", "green", "blue", "orange", "pink"]),
			"size" : sample([8,10,12,14,16], 3),
			"brand" : choice(["Puma", "Reebok", "Adidas", "Bata", "Lotto"])
		}, 
		"slippers" : {
			"color" : choice(["red", "yellow", "green", "blue", "orange", "pink"]),
			"size" : sample([8,10,12,14,16], 3),
			"brand" : choice(["Puma", "Reebok", "Adidas", "Bata", "Lotto"])
		},
		"laptop" : {
			"color" : choice(["red", "yellow", "green", "blue", "orange", "pink"]),
			"size" : choice([11,13,15,16]),
			"memory" : choice(["2GB", "4GB", "8GB", "16GB"]),
			"core" : choice([1,2,3,4]),
			"storage" : choice([128,256,512,1024]),
			"brand" : choice(["Apple", "Dell", "HP", "Dell", "Lenovo"])
		} ,
		"mobile" : {
			"color" : choice(["red", "yellow", "green", "blue", "orange", "pink"]),
			"size" : choice([3.7,4.2,4.7,5.5]),
			"memory" : choice(["2GB", "4GB", "8GB", "16GB"]),
			"core" : choice([1,2,3,4]),
			"storage" : choice([16,32,64,128]),
			"brand" : choice(["Apple", "Sony", "Xiaomi", "LetV", "OnePlus"])
		},
		"television" : {
			"color" : choice(["red", "yellow", "green", "blue", "orange", "pink"]),
			"size" : choice([24,26,28,30,32,34,36]),
			"core" : choice([1,2,3,4]),
			"brand" : choice(["Apple", "Samsung", "Philips", "VU"])
		},
		"speaker" : {
			"color" : choice(["red", "yellow", "green", "blue", "orange", "pink"]),
			"channel" : choice([2,4,6,8]),
			"brand" : choice(["Apple", "Samsung", "Philips", "VU"])
		},
		"pen" : {
			"color" : choice(["red", "yellow", "green", "blue", "orange", "pink"]),
			"size" : choice([0.2,0.4,0.6,0.8]),
			"brand" : choice(["Cello", "Reynolds"])
		} , 
		"pencil" : {
			"color" : choice(["red", "yellow", "green", "blue", "orange", "pink"]),
			"size" : choice([0.2,0.4,0.6,0.8]),
			"brand" : choice(["Apsara", "Natraj"])
		}, 
		"earser" : {
			"size" : choice([2,4,6,8]),
			"brand" : choice(["Apsara", "Natraj"])
		}, 
		"marker" : {
			"color" : choice(["red", "yellow", "green", "blue", "orange", "pink"]),
			"size" : choice([0.2,0.4,0.6,0.8]),
			"brand" : choice(["Cello", "Reynolds"])
		},
		"curtain" : {
			"color" : choice(["red", "yellow", "green", "blue", "orange", "pink"]),
			"size" : choice([0.2,0.4,0.6,0.8]),
			"brand" : choice(["ABC", "XYZ"])

		} , 
		"lamp" : {
			"color" : choice(["red", "yellow", "green", "blue", "orange", "pink"]),
			"power" : sample([10,12,16,20,24,26,28,30,36,40,42,48,50],4)
		}, 
		"chair" : {
			"color" : choice(["red", "yellow", "green", "blue", "orange", "pink"]),
			"size" : choice([0.2,0.4,0.6,0.8]),
			"brand" : choice(["ABC", "XYZ"])
		}, 
		"bed" : {
			"color" : choice(["red", "yellow", "green", "blue", "orange", "pink"]),
			"size" : choice([0.2,0.4,0.6,0.8]),
			"brand" : choice(["ABC", "XYZ"])
		}
	}
	return specification_dict[key]
